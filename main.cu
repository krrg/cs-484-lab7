#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/memory.h>


const unsigned int GRID_DIM = 8192;
const unsigned int LAST = GRID_DIM - 1;

template<typename T>
__device__ inline T* getElement (const int row, const int col, T* grid, const size_t pitch) {
  // The line below looks terrible, but it is stripped out of the CUDA
  // documentation at http://goo.gl/BmZMjq
  return (T*)((char*)grid + row * pitch) + col;
}

__global__ void InitDeviceArrays (float* devGridA, float* devGridB, char* devIsSpecial, size_t pitchA, size_t pitchB, size_t pitchS) {

  int col = blockIdx.x * blockDim.x + threadIdx.x;
  int row = blockIdx.y * blockDim.y + threadIdx.y;

  if (col >= GRID_DIM || row >= GRID_DIM) {
     return;
  }

  if (row == 0) {
    *getElement(row, col, devGridA, pitchA) = 0.0f;
    *getElement(row, col, devIsSpecial, pitchS) = true;
  }
  else if (col == 0 || col == LAST) {
    *getElement(row, col, devGridA, pitchA) = 0.0f;
    *getElement(row, col, devIsSpecial, pitchS) = true;
  }
  else if (row == LAST) {
    *getElement(row, col, devGridA, pitchA) = 100.0f;
    *getElement(row, col, devIsSpecial, pitchS) = true;
  }
  else if (row == 200 && col == 500) {
    *getElement(row, col, devGridA, pitchA) = 100.0f;
    *getElement(row, col, devIsSpecial, pitchS) = true;
  }
  else if (row == 400 && col <= 330) {
    *getElement(row, col, devGridA, pitchA) = 100.0f;
    *getElement(row, col, devIsSpecial, pitchS) = true;
  }
  else {
    *getElement(row, col, devGridA, pitchA) = 50.0f;
    *getElement(row, col, devIsSpecial, pitchS) = false;
  }

  *getElement(row, col, devGridB, pitchB) = *getElement(row, col, devGridA, pitchA);

}

__device__ inline bool isSpecial(char* devIsSpecial, size_t pitch, const int row, const int col) {
  return (bool) (*getElement(row, col, devIsSpecial, pitch));
}

__device__ inline float compute(float* grid, size_t pitch, const int row, const int col) {

  float result =
  (4 * (*getElement(row, col, grid, pitch)) +
  (*getElement(row + 1, col, grid, pitch)) +
  (*getElement(row - 1, col, grid, pitch)) +
  (*getElement(row, col + 1, grid, pitch)) +
  (*getElement(row, col - 1, grid, pitch)))
  * 0.125f;

  return result;
}

__device__ inline bool isUnstable(float* grid, size_t pitch, const int row, const int col) {
  return (
    (*getElement(row, col, grid, pitch)) -
    0.25f * (
      (*getElement(row + 1, col, grid, pitch)) +
      (*getElement(row - 1, col, grid, pitch)) +
      (*getElement(row, col + 1, grid, pitch)) +
      (*getElement(row, col - 1, grid, pitch))
    )
  ) >=  0.1f;
}

__global__ void ComputeIteration (float* devGridA, float* devGridB, char* devIsSpecial, size_t pitchA, size_t pitchB, size_t pitchS) {

  int col = blockIdx.x * blockDim.x + threadIdx.x;
  int row = blockIdx.y * blockDim.y + threadIdx.y;

  if (col >= GRID_DIM || row >= GRID_DIM){
    return;
  }

  if (isSpecial(devIsSpecial, pitchS, row, col)) {
    return;
  }

  // if (abs(compute(devGridA, pitchA, row, col)) > 100) {
  //   printf("ERROR at %d, %d: %f <<<", row, col, compute(devGridA, pitchA, row, col));
  // }

  (*getElement(row, col, devGridB, pitchB)) = compute(devGridA, pitchA, row, col);

}

__global__ void KeepGoing (float* devGridA, float* devGridB, char* devIsSpecial, int* devKeepGoing, size_t pitchA, size_t pitchB, size_t pitchS, const int numBlocksX) {

  int col = blockIdx.x * blockDim.x + threadIdx.x;
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int uniqueThreadId = threadIdx.x + threadIdx.y * blockDim.x;

  // printf(" %d\n", uniqueThreadId);

  /* Load up the sequential array */
  extern __shared__ int sdata[];

  sdata[uniqueThreadId] = 0;

  if (col >= GRID_DIM || row >= GRID_DIM){
    return;
  }

  if (! isSpecial(devIsSpecial, pitchS, row, col)) {
    sdata[uniqueThreadId] = isUnstable(devGridB, pitchB, row, col);
  }

  /* Reduction 1 -- Interleaved addressing */
  // for (int i = 1; i < blockDim.x * blockDim.y; i *= 2) {
  //   if (uniqueThreadId % (2*i) == 0) {
  //     sdata[uniqueThreadId] += (bool)sdata[uniqueThreadId + i];
  //   }
  //   __syncthreads();
  // }

  /* Reduction 2 -- Interleaved addressing with strided index and non-divergent branch */
  // for (int s = 1; s < blockDim.x * blockDim.y; s *= 2) {
  //   int index = 2 * s * uniqueThreadId;
  //
  //   if (index < blockDim.x * blockDim.y) {
  //     sdata[index] += (bool)sdata[index + s];
  //   }
  //   __syncthreads();
  // }

  /* Reduction 3 -- Sequential addressing */
  for (int s = (blockDim.x * blockDim.y) / 2; s > 0; s >>= 1) {
    if (uniqueThreadId < s) {
      sdata[uniqueThreadId] += (bool)sdata[uniqueThreadId + s];
    }
    __syncthreads();
  }

  if (uniqueThreadId == 0) {
    // printf("Block id is: %d %d \n", blockIdx.x, blockIdx.y);
     const int blockNum = numBlocksX * blockIdx.x + blockIdx.y;
     devKeepGoing[blockNum] = sdata[0]; // TODO: CHANGE THIS LINE BACK CHANGE THIS LINE BACK to sdata[0]
  }

}

bool hostKeepGoing (float* devGridA, float* devGridB, char* devIsSpecial, size_t pitchA, size_t pitchB, size_t pitchS) {

  const int THREADS = 64;

  dim3 threadsPerBlock(THREADS, 1);
  dim3 numBlocks(
    GRID_DIM / threadsPerBlock.x,
    GRID_DIM / threadsPerBlock.y
  );


  thrust::device_vector<int> devKeepGoing;
  devKeepGoing.resize(numBlocks.x * numBlocks.y);

  int* rawDevKeepGoing = thrust::raw_pointer_cast(devKeepGoing.data());

  KeepGoing <<<numBlocks, threadsPerBlock, sizeof(int) * threadsPerBlock.x * threadsPerBlock.y>>>(devGridA, devGridB, devIsSpecial, rawDevKeepGoing, pitchA, pitchB, pitchS, numBlocks.x);
  cudaDeviceSynchronize();

  thrust::host_vector<int> hostKeepGoing = devKeepGoing;

  for (auto e: hostKeepGoing) {
    if (e) {
      printf(" %d still left.", e);
      return true;
    }
  }

  return false;
}

int main () {

  dim3 threadsPerBlock(16, 16);
  dim3 numBlocks(
    GRID_DIM / threadsPerBlock.x,
    GRID_DIM / threadsPerBlock.y
  );

  size_t pitchA;
  size_t pitchB;
  size_t pitchS;
  float* devGridA;
  float* devGridB;
  char* devIsSpecial;

  // Use the cudaMallocPitch() to allocate 2d arrays
  cudaMallocPitch(&devGridA, &pitchA, sizeof(float) * GRID_DIM, GRID_DIM);
  cudaMallocPitch(&devGridB, &pitchB, sizeof(float) * GRID_DIM, GRID_DIM);
  cudaMallocPitch(&devIsSpecial, &pitchS, sizeof(char) * GRID_DIM, GRID_DIM);


  InitDeviceArrays<<<numBlocks, threadsPerBlock>>>(devGridA, devGridB, devIsSpecial, pitchA, pitchB, pitchS);
  cudaDeviceSynchronize();

  for (int i = 0; true; i++) {
    ComputeIteration<<<numBlocks, threadsPerBlock>>>(devGridA, devGridB, devIsSpecial, pitchA, pitchB, pitchS);
    cudaDeviceSynchronize();
    bool keepGoing = hostKeepGoing(devGridA, devGridB, devIsSpecial, pitchA, pitchB, pitchS);

    std::swap(devGridA, devGridB);
    std::swap(pitchA, pitchB);
    printf("DONE [%d]\n", i);

    if (!keepGoing) {
      break;
    }
  }

  cudaFree(devGridA);
  cudaFree(devGridB);
  cudaFree(devIsSpecial);

  cudaDeviceSynchronize();
  return 0;
}
